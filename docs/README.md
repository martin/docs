# Onion MkDocs: Material template for Tor documentation

This is a [MkDocs][] documentation template for [Tor][]-related projects and
based on the [Material theme][].

[Tor]: https://torproject.org
[MkDocs]: https://www.mkdocs.org/
[Material theme]: https://squidfunk.github.io/mkdocs-material/

## Repository

Check the repository at
[https://gitlab.torproject.org/tpo/web/onion-mkdocs](https://gitlab.torproject.org/tpo/web/onion-mkdocs).

## Requirements

Onion MkDocs relies the following software:

* [MkDocs][] as the web frameworks.
* [Pipenv](https://pipenv.pypa.io/en/latest/),
  [pip](https://pypi.org/project/pip/) or other package manager to handle
  Python dependencies.
* The [Material theme][] for all the styling.

Optional dependencies:

* [GNU Make](https://www.gnu.org/software/make) for building.

Example installation procedures tested on Debian Bullseye are available:

* [scripts/onion-mkdocs-provision-build](https://gitlab.torproject.org/tpo/web/onion-mkdocs/-/blob/main/scripts/onion-mkdocs-provision-build):
  installs all the needed dependencies to build Onion MkDocs websites.
* [scripts/onion-mkdocs-provision-host](https://gitlab.torproject.org/tpo/web/onion-mkdocs/-/blob/main/scripts/onion-mkdocs-provision-host):
  provision the hosting environment to serve Onion MkDocs websites, including
  a [Tor Onion Service](https://community.torproject.org/onion-services/).

## Repository setup

This repository is ready to be used in any existing repository.
It can be installed in a number of ways as follows.

### Forking the project

Simply fork this project and change whatever you need.

### As a Git submodule

You can simply put it as a Git submodule somewhere, like in a `vendor` (or
`vendors`) folder of your project:

    mkdir -p vendor && \
    git submodule add --depth 1 https://gitlab.torproject.org/tpo/web/onion-mkdocs vendor/onion-mkdocs

Then use symbolic links to reference all the needed files from the root of your
repository.

If you plan to use either GitLab CI/CD or GitHub Actions to build your
documentation, make sure to at least copy/adapt the corresponding files from
the submodule. You can't just symlink those since the submodule won't be
accessible when GitLab and GitHub looks for CI/CD or Actions files. The
provided GitLab CI/CD and GitHub actions configuration takes care of
initializing the submodule during build time, but they should be available in
the main repo in order to do so.

### Manually copying

Even simpler, copy all the relevant files from this repository into your project.

### Other ways

* `git-subtree(1)`.
* ?

## Compiling

Once all dependencies are installed and the required files are in place in your repository, just
type the following to build your Onion MkDocs website:

    vendor/onion-mkdocs/scripts/onion-mkdocs-build

Or, if you have setup the proper Makefile targets:

    make compile

## Serve

As an alias to `mkdocs(1)` `serve` command, type

    vendor/onion-mkdocs/scripts/onion-mkdocs-serve

Or, if you have setup the proper Makefile targets:

    make serve

## Watching

A basic "watch" mode to rebuild your site whenever the docs change (like a live edit mode), use

    vendor/onion-mkdocs/scripts/onion-mkdocs-watch

Or, if you have setup the proper Makefile targets:

    make watch

## Formatting support

### MkDocs extensions

Supports all [default MkDocs extensions](https://www.mkdocs.org/user-guide/configuration/#markdown_extensions).

### Tasklists

Supports [Material theme][]'s [tasklist](https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown-extensions/?h=#tasklist),
which is compatible with [GitLab Flavored Markdown (GLFM)](https://docs.gitlab.com/ee/user/markdown.html).

### Diagrams

Onion MkDocs also comes with [Diagrams](https://squidfunk.github.io/mkdocs-material/reference/diagrams/) support:

```mermaid
graph LR
  Write --> Build --> Share
```

### Other formatting plugins

All [other markdown extensions][] from the [Material theme][] are also
available by additional configuration.

[other markdown extensions]: https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown-extensions/

## GitLab pages support

Onion MkDocs supports GitLab pages out-of-the box with a [simple GitLab CI/CD
configuration][].

[simple GitLab CI/CD configuration]: https://gitlab.torproject.org/tpo/web/onion-mkdocs/-/blob/main/.gitlab-ci-pages.yml

## GitHub pages support

Onion MkDocs also supports GitHub pages out-of-the box with a [simple GitHub CI/CD
configuration][].

[simple GitHub CI/CD configuration]: https://gitlab.torproject.org/tpo/web/onion-mkdocs/-/blob/main/.github/gh-pages.yml

## Onionshare support

It's possible to share your local Onion MkDocs build with others by using [OnionShare][].
Ensure you have a working [onionshare-cli installation][], the proper Makefile targets
and then type:

    make share

Can even be combined with `make watch` to keep the shared site always up-to-date (untested).

[OnionShare]: https://onionshare.org
[onionshare-cli installation]: https://docs.onionshare.org/2.6/en/advanced.html#command-line-interface

## Live examples

Check out some live examples of Onion MkDocs config in action:

* [The Onion Plan](https://tpo.pages.torproject.net/onion-services/onionplan/).
* [The Oniongroove](https://tpo.pages.torproject.net/onion-services/oniongroove/).
* [Onionprobe](https://tpo.pages.torproject.net/onion-services/onionprobe/).

## Index

[TOC]
